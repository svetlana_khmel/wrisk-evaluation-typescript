import * as actionTypes from '../actions/actionTypes';
import { SystemState } from '../interfaces';

const initialState: SystemState = {
  details: 0,
  locationData: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_TIMELINE:
      const { data, locationData } = action.payload;
      return { ...state, timelines: data, locationData: [...state.locationData, locationData] };
    case actionTypes.SET_DETAILS:
      return { ...state, details: action.payload };

    default:
      return state;
  }
};

export default reducer;
