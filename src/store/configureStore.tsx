import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';

const middlewares = applyMiddleware(thunk);

export const configureStore = () => createStore(rootReducer, middlewares);
//export type AppDispatch = typeof configureStore.dispatch
export type RootState = ReturnType<typeof rootReducer>;
