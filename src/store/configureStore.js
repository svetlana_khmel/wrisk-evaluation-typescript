import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
var middlewares = applyMiddleware(thunk);
export var configureStore = function () { return createStore(rootReducer, middlewares); };
