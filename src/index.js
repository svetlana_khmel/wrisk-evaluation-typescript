import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from './store/configureStore';
import { ThemeProvider } from './components/ThemeProvider';
import App from './App';
var store = configureStore();
ReactDOM.render(React.createElement(Provider, { store: store },
    React.createElement(ThemeProvider, null,
        React.createElement(App, null))), document.getElementById('root'));
