import React, { useEffect, lazy, useCallback, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getIp } from './actions';
import { ThemeContext } from './components/ThemeProvider';
import { RootState } from './interfaces';

import Loader from './components/Loader';
import ThemeToggler from './components/ThemeToggler';

import City from './components/City';
import Date from './components/Date';
import Dashboard from './components/Dashboard';
import Details from './components/Details';

import './styles/app.scss';

const App: React.FC = () => {
  const dispatch = useDispatch();
  const timelines = useSelector((state: RootState) => state.timelines);
  const locationData = useSelector((state: RootState) => state.locationData);
  const { theme } = useContext(ThemeContext);

  useEffect(() => {
    dispatch(getIp());
  }, []);

  if (!timelines) return <Loader />;
  return (
    <div className={`container ${theme} `}>
      <ThemeToggler />
      <City city={locationData[0].city} />
      <Date />
      <Dashboard intervals={timelines} />
      <Details />
    </div>
  );
};

export default App;
