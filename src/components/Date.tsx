import React from 'react';
import { getDayTomorrow, getCurrentMonth } from '../Utils';

const Date: React.FC = () => {
  const day = getDayTomorrow();
  const month = getCurrentMonth();
  return <>{`${day} ${month}`}</>;
};

export default Date;
