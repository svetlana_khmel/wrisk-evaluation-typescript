import React from 'react';

const City: React.FC<{ city: string }> = ({ city }) => {
  return <div className="city">{city}</div>;
};

export default City;
