"use strict";
// import React, { useState, useEffect} from 'react';
// import './styles/style.scss';
//
//
// export interface IData {
//     body: string;
//     id: number;
//     userId: number;
//     title: string;
// }
// const App = ():JSX.Element => {
//     const [data, setData] = useState<IData[]>([]);
//     useEffect(() => {
//         const url = `https://jsonplaceholder.typicode.com/posts/`;
//         fetch(url)
//             .then((resp) => {
//                 return resp.json();
//             })
//             .then((data) => {
//                 console.log(data);
//                 setData(data)
//             })
//     }, []);
//
//     const renderData = () => {
//         if (!data)  return (<div>Loading...</div>);
//         return data.map(el => {
//             return (<div className="post" key={el.id+el.title}>
//                 <div className="post-title">{el.title}</div>
//                 <div key={el.id} className="post-body">{el.body}</div>
//             </div> )
//         })
//     };
//
//     return (<div className="container">{renderData()}</div>);
// };
//
// export default App;
