import React, { memo } from 'react';

import '../styles/loader.scss';

const Loader: React.FC = () => <div className="lds-dual-ring">...</div>;

export default memo(Loader);
