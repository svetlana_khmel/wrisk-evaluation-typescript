import React, { memo } from 'react';
import PreviewCard from './PreviewCard';
import { ArrayOfIntervals } from '../interfaces';

import '../styles/timeline.scss';

const Timeline: React.FC<ArrayOfIntervals> = ({ intervals }) => {
  const renderCards = () =>
    intervals.map((el, index) => (
      <PreviewCard key={el.startTime} startTime={el.startTime} values={el.values} index={index} />
    ));

  return <div className="timeline">{renderCards()}</div>;
};

export default memo(Timeline);
