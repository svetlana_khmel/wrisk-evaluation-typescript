import React from 'react';
import { useSelector } from 'react-redux';
import { timeToHours, mapCodeToImg } from '../Utils';
import { riskEstimation, riskCalculation } from '../Utils/riskManagement';
import '../styles/details.scss';
var Details = function () {
    var timelines = useSelector(function (state) { return state.timelines; });
    var details = useSelector(function (state) { return state.details; });
    var data = timelines[details];
    var _a = data.values, weatherCode = _a.weatherCode, temperature = _a.temperature, precipitationProbability = _a.precipitationProbability, visibility = _a.visibility, windSpeed = _a.windSpeed;
    var theme = 'color';
    var hours = timeToHours(data.startTime);
    var imgUrl = mapCodeToImg(weatherCode);
    var risk = riskCalculation(temperature, precipitationProbability, visibility, windSpeed);
    return (React.createElement("div", { className: "details" },
        React.createElement("div", { className: "time" }, hours),
        React.createElement("div", { style: { backgroundImage: "url(../public/assets/" + theme + "/" + imgUrl + ".svg)" }, className: "img" }),
        React.createElement("div", { className: "temperature" }, temperature),
        React.createElement("div", { className: "precipitation-probability" },
            "Precipitation probability: ",
            precipitationProbability),
        React.createElement("div", { className: "visibility" },
            "Visibility: ",
            visibility),
        React.createElement("div", { className: "wind-speed" },
            "Wind speed: ",
            windSpeed),
        React.createElement("div", { className: "risk" },
            "Risk estimated: ",
            risk,
            ", ",
            riskEstimation(risk),
            ' ')));
};
export default Details;
