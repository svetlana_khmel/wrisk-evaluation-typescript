import React, { useState } from "react";
export var ThemeContext = React.createContext({});
export var ThemeProvider = function (_a) {
    var children = _a.children;
    var _b = useState("color"), theme = _b[0], setTheme = _b[1];
    var toggleTheme = function () {
        setTheme(theme === "color" ? "black" : "color");
    };
    return (React.createElement(ThemeContext.Provider, { value: { theme: theme, toggleTheme: toggleTheme } }, children));
};
