import React, { useContext, memo } from 'react';
import { ThemeContext } from './ThemeProvider';
import '../styles/toggler.scss';
var ThemeToggler = function () {
    var _a = useContext(ThemeContext), theme = _a.theme, toggleTheme = _a.toggleTheme;
    return (React.createElement("button", { className: "toggler", onClick: toggleTheme },
        "Switch to ",
        theme === 'color' ? 'black' : 'color',
        " mode"));
};
export default memo(ThemeToggler);
