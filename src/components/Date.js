import React from 'react';
import { getDayTomorrow, getCurrentMonth } from '../Utils';
var Date = function () {
    var day = getDayTomorrow();
    var month = getCurrentMonth();
    return (React.createElement(React.Fragment, null, day + " " + month));
};
export default Date;
