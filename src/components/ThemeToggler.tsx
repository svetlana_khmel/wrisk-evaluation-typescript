import React, { useContext, memo } from 'react';
import { ThemeContext } from './ThemeProvider';

import '../styles/toggler.scss';

const ThemeToggler: React.FC = () => {
  const { theme, toggleTheme } = useContext(ThemeContext);

  return (
    <button className="toggler" onClick={toggleTheme}>
      Switch to {theme === 'color' ? 'black' : 'color'} mode
    </button>
  );
};

export default memo(ThemeToggler);
