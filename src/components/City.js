import React from 'react';
var City = function (_a) {
    var city = _a.city;
    return React.createElement("div", { className: "city" }, city);
};
export default City;
