import React from 'react';
import { useSelector } from 'react-redux';
import { timeToHours, mapCodeToImg } from '../Utils';
import { riskEstimation, riskCalculation } from '../Utils/riskManagement';
import { RootState } from '../interfaces';

import '../styles/details.scss';

const Details: React.FC = () => {
  const timelines = useSelector((state: RootState) => state.timelines);
  const details = useSelector((state: RootState) => state.details);
  const data = timelines[details];

  const { weatherCode, temperature, precipitationProbability, visibility, windSpeed } = data.values;
  const theme = 'color';
  const hours = timeToHours(data.startTime);
  const imgUrl = mapCodeToImg(weatherCode);
  const risk = riskCalculation(temperature, precipitationProbability, visibility, windSpeed);

  return (
    <div className="details">
      <div className="time">{hours}</div>
      <div style={{ backgroundImage: `url(../public/assets/${theme}/${imgUrl}.svg)` }} className="img"></div>
      <div className="temperature">{temperature}</div>
      <div className="precipitation-probability">Precipitation probability: {precipitationProbability}</div>
      <div className="visibility">Visibility: {visibility}</div>
      <div className="wind-speed">Wind speed: {windSpeed}</div>
      <div className="risk">
        Risk estimated: {risk}, {riskEstimation(risk)}{' '}
      </div>
    </div>
  );
};

export default Details;
