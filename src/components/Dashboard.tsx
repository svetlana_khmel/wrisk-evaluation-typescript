import React from 'react';
import { ArrayOfIntervals } from '../interfaces';
import Timeline from './Timeline';

const Dashboard: React.FC<ArrayOfIntervals> = ({ intervals }) => {
  const args = { intervals };

  return (
    <div className="overflow">
      <Timeline {...args} />
    </div>
  );
};

export default Dashboard;
