var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from 'react';
import Timeline from './Timeline';
var Dashboard = function (_a) {
    var intervals = _a.intervals;
    var args = { intervals: intervals };
    return (React.createElement("div", { className: "overflow" },
        React.createElement(Timeline, __assign({}, args))));
};
export default Dashboard;
