import React, { memo } from 'react';
import PreviewCard from './PreviewCard';
import '../styles/timeline.scss';
var Timeline = function (_a) {
    var intervals = _a.intervals;
    var renderCards = function () { return intervals.map(function (el, index) { return (React.createElement(PreviewCard, { key: el.startTime, startTime: el.startTime, values: el.values, index: index })); }); };
    return React.createElement("div", { className: "timeline" }, renderCards());
};
export default memo(Timeline);
