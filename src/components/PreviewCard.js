import React, { useCallback, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setDetails } from '../actions';
import { ThemeContext } from './ThemeProvider';
import { timeToHours, mapCodeToImg } from '../Utils';
var PreviewCard = function (_a) {
    var startTime = _a.startTime, values = _a.values, index = _a.index;
    var details = useSelector(function (state) { return state.details; });
    var theme = useContext(ThemeContext).theme;
    var weatherCode = values.weatherCode, temperature = values.temperature;
    var hours = timeToHours(startTime);
    var imgUrl = mapCodeToImg(weatherCode);
    var dispatch = useDispatch();
    var showDetails = useCallback(function () {
        dispatch(setDetails(index));
    }, []);
    return (React.createElement("div", { className: "card " + (details === index ? 'active' : ''), onClick: showDetails },
        React.createElement("div", { className: "time" }, hours),
        React.createElement("div", { style: { backgroundImage: "url(../public/assets/" + theme + "/" + imgUrl + ".svg)" }, className: "img" }),
        React.createElement("div", { className: "temperature" }, temperature)));
};
export default PreviewCard;
