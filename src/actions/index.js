import axios from 'axios';
import * as actionTypes from './actionTypes';
import { getTime, getTimezone } from '../Utils';
import { urlIp, ipstackBaseUrl, climatCellBase, iostackAccessKey, climatCellSectretKey } from '../Api/settings';
var setTimeline = function (data, locationData) { return ({
    type: actionTypes.SET_TIMELINE,
    payload: { data: data, locationData: locationData }
}); };
var setDetails = function (index) { return ({ type: actionTypes.SET_DETAILS, payload: index }); };
var getIp = function () { return function (dispatch) {
    axios
        .get(urlIp)
        .then(function (data) {
        console.log(data);
        dispatch(getCity(data.data));
    })
        .catch(function (error) { return console.log(error); });
}; };
var getCity = function (ip) { return function (dispatch) {
    axios
        .get(ipstackBaseUrl + "/" + ip + "?access_key=" + iostackAccessKey)
        .then(function (locationData) {
        dispatch(getTimeline(locationData));
    })
        .catch(function (err) { return console.log('Get city err, ', err); });
}; };
var getTimeline = function (locationData) { return function (displatch) {
    var _a = locationData.data, latitude = _a.latitude, longitude = _a.longitude;
    var startTime = getTime();
    var endTime = getTime('future');
    var timezone = getTimezone();
    var url = climatCellBase + "/timelines?location=" + latitude + "," + longitude + "&startTime=" + startTime + "&endTime=" + endTime + "&fields=temperature&fields=weatherCode&fields=visibility&fields=precipitationType&fields=precipitationProbability&fields=windSpeed&timesteps=1h&units=metric&timezone=" + timezone + "&apikey=" + climatCellSectretKey;
    axios
        .get(url)
        .then(function (data) { return displatch(setTimeline(data.data.data.timelines[0].intervals, locationData.data)); })
        .catch(function (err) { return console.error(err); });
}; };
export { getIp, setDetails };
