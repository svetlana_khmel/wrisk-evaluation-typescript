import axios from 'axios';
import { Action } from 'redux';
import * as actionTypes from './actionTypes';
import { getTime, getTimezone } from '../Utils';
import { Location, CityData, ActionTypes, SetTimelineAction, SetDetailsAction } from '../interfaces';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '../store/configureStore';

import { urlIp, ipstackBaseUrl, climatCellBase, iostackAccessKey, climatCellSectretKey } from '../Api/settings';

declare module 'axios' {
  export interface AxiosRequestConfig {
    handlerEnabled: boolean;
  }
}

const setTimeline = (data, locationData): SetTimelineAction => ({
  type: actionTypes.SET_TIMELINE,
  payload: { data, locationData }
});
const setDetails = (index: number): SetDetailsAction => ({ type: actionTypes.SET_DETAILS, payload: index });

const getIp = () => dispatch => {
  axios
    .get<string>(urlIp)
    .then(data => {
      console.log(data);
      dispatch(getCity(data.data));
    })
    .catch(error => console.log(error));
};

const getCity = (ip: string): ThunkAction<void, RootState, unknown, Action<string>> => dispatch => {
  axios
    .get<any>(`${ipstackBaseUrl}/${ip}?access_key=${iostackAccessKey}`)
    .then(locationData => {
      dispatch(getTimeline(locationData));
    })
    .catch(err => console.log('Get city err, ', err));
};

const getTimeline = locationData => displatch => {
  const { latitude, longitude } = locationData.data;
  const startTime = getTime();
  const endTime = getTime('future');
  const timezone = getTimezone();
  const url = `${climatCellBase}/timelines?location=${latitude},${longitude}&startTime=${startTime}&endTime=${endTime}&fields=temperature&fields=weatherCode&fields=visibility&fields=precipitationType&fields=precipitationProbability&fields=windSpeed&timesteps=1h&units=metric&timezone=${timezone}&apikey=${climatCellSectretKey}`;

  axios
    .get<any>(url)
    .then(data => displatch(setTimeline(data.data.data.timelines[0].intervals, locationData.data)))
    .catch(err => console.error(err));
};

export { getIp, setDetails };
