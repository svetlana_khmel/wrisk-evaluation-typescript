import React, { useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getIp } from './actions';
import { ThemeContext } from './components/ThemeProvider';
import Loader from './components/Loader';
import ThemeToggler from './components/ThemeToggler';
import City from './components/City';
import Date from './components/Date';
import Dashboard from './components/Dashboard';
import Details from './components/Details';
import './styles/app.scss';
var App = function () {
    var dispatch = useDispatch();
    var timelines = useSelector(function (state) { return state.timelines; });
    var locationData = useSelector(function (state) { return state.locationData; });
    var theme = useContext(ThemeContext).theme;
    useEffect(function () {
        dispatch(getIp());
    }, []);
    if (!timelines)
        return React.createElement(Loader, null);
    return (React.createElement("div", { className: "container " + theme + " " },
        React.createElement(ThemeToggler, null),
        React.createElement(City, { city: locationData[0].city }),
        React.createElement(Date, null),
        React.createElement(Dashboard, { intervals: timelines }),
        React.createElement(Details, null)));
};
export default App;
