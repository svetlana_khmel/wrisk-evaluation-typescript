import * as actionTypes from '../actions/actionTypes';

export interface RootState {
  details: number;
  timelines: Array<Interval>;
  locationData: Location;
}

export interface SystemState {
  details: number;
  locationData: Array<Interval>;
}

export interface SetTimelineAction {
  type: typeof actionTypes.SET_TIMELINE;
  payload: {
    data: any;
    locationData: Array<Interval>;
  };
}

export interface SetDetailsAction {
  type: typeof actionTypes.SET_DETAILS;
  payload: number;
}

export type ActionTypes = SetTimelineAction | SetDetailsAction;

export interface Values {
  precipitationProbability: number;
  precipitationType: number;
  temperature: number;
  visibility: number;
  weatherCode: number;
  windSpeed: number;
}
export interface Interval {
  startTime: string;
  values: Values;
}

export interface ArrayOfIntervals {
  intervals: Array<Interval>;
}

export interface Location {
  calling_code: string;
  capital: string;
  country_flag: string;
  country_flag_emoji: string;
  country_flag_emoji_unicode: string;
  geoname_id: number;
  is_eu: boolean;
  languages: string[];
}

export interface CityData {
  city: string;
  continent_code: string;
  continent_name: string;
  country_code: string;
  country_name: string;
  ip: string;
  latitude: number;
  location: Location;
  longitude: number;
  region_code: string;
  region_name: string;
  type: string;
  zip: string;
}

export interface CardInfo {
  startTime: string;
  values: Values;
  index: number;
}
